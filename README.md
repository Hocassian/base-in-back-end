# base-in-back-end
服务端基础框架

# Run
- 修改数据库链接信息

- 启动应用
```
netstat -nplt
kill xxx
nohup java -jar ./base-in-back-end-0.0.1-SNAPSHOT.jar --server.port=9090 --spring.profiles.active=prod >nohup.out&
tail -f -n 200 nohup.out

```

- 访问
http://localhost:8080/xxx/back-end/swagger-ui.html
